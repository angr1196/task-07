package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DBManager {
    private static final Lock CONNECTION_LOCK = new ReentrantLock();
    private static DBManager instance;
    private static Connection connection;

    private DBManager() {
        connection = getConnection();
    }

    public static Connection getConnection() {

        try {
            InputStream in = new FileInputStream("app.properties");
            Properties prop = new Properties();
            prop.load(in);
            return
                    DriverManager.getConnection(prop.getProperty("connection.url"));
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private static void close(Statement st) {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void close(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void setAutocommit() {
        try {
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<User> findAllUsers() throws DBException {
        Statement ps = null;
        ResultSet rs = null;
        List<User> users = new ArrayList<>();
        try {
            CONNECTION_LOCK.lock();
            ps = connection.createStatement();
            rs = ps.executeQuery("SELECT * FROM users");
            while (rs.next()) {
                User user = new User();
                users.add(user);
                user.setId(rs.getInt(1));
                user.setLogin(rs.getString(2));
            }
        } catch (Exception e) {
            throw new DBException("findAllUsers db error", e);

        } finally {
            close(rs);
            close(ps);
            CONNECTION_LOCK.unlock();
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        PreparedStatement ps = null;
        ResultSet id = null;
        try {
            CONNECTION_LOCK.lock();


            ps = connection.prepareStatement("INSERT INTO users VALUES (DEFAULT, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, user.getLogin());
            if (ps.executeUpdate() != 1) {
                return false;
            }
            id = ps.getGeneratedKeys();
            if (id.next()) {
                int idField = id.getInt(1);
                user.setId(idField);
            }
        } catch (Exception e) {
            System.out.println("insertUser db error"+ e.getMessage());
            return false;

        } finally {
            close(id);
            close(ps);
            CONNECTION_LOCK.unlock();
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {

        PreparedStatement ps = null;
        ResultSet id = null;
        try {
            CONNECTION_LOCK.lock();
            ps = connection.prepareStatement("DELETE FROM users WHERE id =?");
            for (int i = 0; i < users.length; i++) {
                ps.setString(1, users[i].getLogin());
                if (ps.executeUpdate() != 1) {
                    return false;
                }
            }
        } catch (Exception e) {
            return  false;

        } finally {
            close(id);
            close(ps);
            CONNECTION_LOCK.unlock();
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        User user = null;
        try {
            CONNECTION_LOCK.lock();

            ps = connection.prepareStatement("SELECT * FROM users WHERE login=?");
            ps.setString(1, login);
            rs = ps.executeQuery();
            if (rs.next()) {
                user = new User();
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
            }
        } catch (SQLException e) {
            throw new DBException("getUser db error", e);
        } finally {
            close(rs);
            close(ps);
            CONNECTION_LOCK.unlock();
        }
        return user;

    }

    public Team getTeam(String name) throws DBException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Team team = null;
        try {
            CONNECTION_LOCK.lock();
            ps = connection.prepareStatement("SELECT * FROM teams WHERE name=?");
            ps.setString(1, name);
            rs = ps.executeQuery();
            if (rs.next()) {
                team = new Team();
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
            }
        } catch (SQLException ex) {
            throw new DBException("getTeam db error", ex);
        } finally {
            close(rs);
            close(ps);
            CONNECTION_LOCK.unlock();
        }
        return team;

    }

    public List<Team> findAllTeams() throws DBException {
        Statement ps = null;
        ResultSet rs = null;
        List<Team> teams = new ArrayList<>();
        try {
            CONNECTION_LOCK.lock();
            ps = connection.createStatement();
            rs = ps.executeQuery("SELECT * FROM teams");
            while (rs.next()) {
                Team team = new Team();
                teams.add(team);
                team.setId(rs.getInt(1));
                team.setName(rs.getString(2));
            }
        } catch (Exception e) {
            throw new DBException("findAllUsers db error", e);

        } finally {
            close(rs);
            close(ps);
            CONNECTION_LOCK.unlock();
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        PreparedStatement ps = null;
        ResultSet id = null;
        try {
            CONNECTION_LOCK.lock();
            ps = connection.prepareStatement("INSERT INTO teams VALUES (DEFAULT, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, team.getName());
            if (ps.executeUpdate() != 1) {
                return false;
            }
            id = ps.getGeneratedKeys();
            if (id.next()) {
                int idField = id.getInt(1);
                team.setId(idField);
            }
        } catch (Exception e) {
            throw new DBException("insertTeam db error", e);

        } finally {
            close(id);
            close(ps);
            CONNECTION_LOCK.unlock();
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            CONNECTION_LOCK.lock();
            connection.setAutoCommit(false);
            ps = connection.prepareStatement("INSERT INTO users_teams VALUES(?,?)");
            for (Team t : teams) {
                ps.setInt(1, user.getId());
                ps.setInt(2, t.getId());
                ps.addBatch();
            }
            int[] usersTeams = ps.executeBatch();
            for (int i : usersTeams) {
                if (i != 1) {
                    return false;
                }
            }
            connection.commit();
            return true;
        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            throw new DBException("setTeam db error", ex);
        } finally {
            close(rs);
            close(ps);
            setAutocommit();
            CONNECTION_LOCK.unlock();
        }

    }

    public List<Team> getUserTeams(User user) throws DBException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Team> teams = new ArrayList<>();
        try {
            CONNECTION_LOCK.lock();
            ps = connection.prepareStatement("SELECT t.id, t.name FROM users_teams ut\n"
                    + "JOIN users u ON ut.user_id = u.id\n"
                    + "JOIN teams t ON ut.team_id = t.id\n"
                    + "WHERE u.id = ?");
            ps.setInt(1, user.getId());
            rs = ps.executeQuery();
            while (rs.next()) {
                Team team = new Team();
                teams.add(team);
                team.setId(rs.getInt(1));
                team.setName(rs.getString(2));
            }
        } catch (Exception e) {
            throw new DBException("GetTeam db error", e);

        } finally {
            close(rs);
            close(ps);
            CONNECTION_LOCK.unlock();
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        PreparedStatement ps = null;
        ResultSet id = null;
        try {
            CONNECTION_LOCK.lock();
            ps = connection.prepareStatement("DELETE FROM teams WHERE name =?");
            ps.setString(1, team.getName());
            if (ps.executeUpdate() != 1) {
                return false;
            }
        } catch (Exception e) {
            throw new DBException("DeleteUser db error", e);

        } finally {
            close(id);
            close(ps);
            CONNECTION_LOCK.unlock();
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        PreparedStatement ps = null;
        ResultSet id = null;
        try {
            CONNECTION_LOCK.lock();
            ps = connection.prepareStatement("UPDATE teams SET name =? WHERE id = ?");
            ps.setString(1, team.getName());
            ps.setInt(2, team.getId());
            if (ps.executeUpdate() != 1) {
                return false;
            }
        } catch (Exception e) {
            throw new DBException("UpdeateTeam db error", e);

        } finally {
            close(id);
            close(ps);
            CONNECTION_LOCK.unlock();
        }
        return true;
    }

}
