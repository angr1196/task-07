package com.epam.rd.java.basic.task7.db.entity;

public class Team {

	private int id;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public static Team createTeam(String name) {
		Team team = new Team();
		team.setName(name);
		return team;
	}
	@Override
	public boolean equals(Object o){
		if(o==null|| o.getClass() != this.getClass()){
			return false;
		}
		else{
			Team t = (Team)o;
			return name.equals(t.name);
		}
	}
	@Override
	public String toString(){
		return name;
	}

}
