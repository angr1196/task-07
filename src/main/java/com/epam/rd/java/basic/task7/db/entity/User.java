package com.epam.rd.java.basic.task7.db.entity;

public class User {

	private int id;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
		User user = new User();
		user.setLogin(login);
		return user;
	}

	@Override
	public boolean equals(Object o){
		if(o==null|| o.getClass() != this.getClass()){
			return false;
		}
		else{
			User u = (User)o;
			return login.equals(u.login);
		}
	}
	@Override
	public String toString(){
		return login;
	}

}
