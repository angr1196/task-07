DROP database IF EXISTS test2db;

CREATE database test2db;

USE test2db;

CREATE TABLE users (
	id INT PRIMARY KEY auto_increment,
	login VARCHAR(10) UNIQUE
);

CREATE TABLE teams (
	id INT PRIMARY KEY auto_increment,
	name VARCHAR(10)
);

CREATE TABLE users_teams (
	user_id INT,
	team_id INT,
	 UNIQUE (user_id, team_id),
	 CONSTRAINT FK_1 FOREIGN KEY(user_id) REFERENCES users(id) on delete cascade,
    CONSTRAINT FK_2 FOREIGN KEY(team_id) REFERENCES teams(id) on delete cascade);

INSERT INTO users VALUES (DEFAULT, 'ivanov');
INSERT INTO teams VALUES (DEFAULT, 'teamA');

SELECT * FROM users;
SELECT * FROM teams;
SELECT * FROM users_teams;

